// Object
// - a data type that is used to represent real world objects

// Creating objects using object initializer/literal notation
/*
	- Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB,
			keyC: valueC
		}
*/

let cellPhone = {
	name: 'Nokia 3315',
	manufactureDate: 1999
}

console.log("Result from creating objects with initializers/literal notation");
console.log(cellPhone);
console.log(typeof cellPhone);

// Creating objects using a constructor function

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// Creating a unique instance of the Laptop object
// Using "new" operator creates an instance of an object

let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using objects constructors");
console.log(laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.log("Result from creating objects using objects constructors");
console.log(myLaptop);

let oldLaptop = Laptop('Alienware', 2016);
console.log("Result from creating objects without the new keyword:");
console.log(oldLaptop);

// Create empty objects 
let computer = {};
let myComputer = new Object();

// Accessing Object Properties
// Using dot notation
console.log('Result from dot notation:' + myLaptop.name);
console.log('Result from dot notation:' + laptop.manufactureDate);

// Using square bracket notation
console.log('Result from square bracket notation:' + laptop['name']);

// Accessing array object

let array = [laptop, myLaptop];

console.log(array[0]['name']);
console.log(array[1].name);